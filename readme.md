# Zap : tracer.js

This package installs the client side module for Zap to diagnose, debug and fix bugs in client side JavaScript.

##### Installation

Install the package using npm if you already use a module bundler like webpack, brunch or gulp by running :

```
npm install zap-tracer.js
```

This will install the module globally in your code.

Now you need to initialize the module by including this code in your main JS file :

```
zapTracer.init('key')
```

The 'key' here can be obtained from : ~Link to key page~

To track errors manually, you can use the following in your code :

```
zapTracer.track(error)
```

Here **error** is a JS error object defined as per W3C standards.
