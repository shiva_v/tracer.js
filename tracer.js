const zapTracer = (function () {

  function generateUUID() {
    var d = new Date().getTime();
    var d2 = (performance && performance.now && (performance.now()*1000)) || 0;
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;
        if(d > 0){
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }

  var utilities = {
    transmit: function(payload) {
      var sessionID = generateUUID();
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "http://localhost:3000/v1/events/data", true);
      // xhr.setRequestHeader('Content-Type', 'application/json');
      // xhr.setRequestHeader('x-session-id', sessionID);
      // xhr.send(JSON.stringify(payload));
      console.log('Transmitting payload', payload);
    }
  }

  return {
    init: function(key) {
      if(typeof window !== "undefined" && typeof window.console !== "undefined") {
        window.onerror = function (msg, url, lineNo, columnNo, error) {
          var string = msg.toLowerCase();
          var substring = "script error";
          if (string.indexOf(substring) > -1){
            console.log('Script Error: See Browser Console for Detail');
            // Generic script error
          } else {
            var message = [
              'Message: ' + msg,
              'URL: ' + url,
              'Line: ' + lineNo,
              'Column: ' + columnNo,
              'Error object: ' + JSON.stringify(error)
            ].join(' - ');

            console.log(message);
          }

          return false;
        };
        var console=(function(oldConsole){
          var messagePayload = {};
            return {
                log: function(text){
                    oldConsole.log(text);
                    messagePayload = {
                      type : "log",
                      message : text,
                      metadata : ""
                    }
                    utilities.transmit(messagePayload);
                    // Your code
                },
                info: function (text) {
                    messagePayload = {
                      type : "info",
                      message : text,
                      metadata : ""
                    }
                    utilities.transmit(messagePayload);
                    oldConsole.info(text);
                },
                warn: function (text) {
                    messagePayload = {
                      type : "warn",
                      message : text,
                      metadata : ""
                    }
                    utilities.transmit(messagePayload);
                    oldConsole.warn(text);
                    // Your code
                },
                error: function (text) {
                    messagePayload = {
                      type : "error",
                      message : text,
                      metadata : ""
                    }
                    utilities.transmit(messagePayload);
                    oldConsole.error(text);
                    // Your code
                }
            };
        }(window.console));

        function captureNetworkRequests(e) {
            var capture_network_request = [];
            var capture_resource = performance.getEntriesByType("resource");
            for (var i = 0; i < capture_resource.length; i++) {
                if (capture_resource[i].initiatorType == "xmlhttprequest" || capture_resource[i].initiatorType == "script" || capture_resource[i].initiatorType == "img") {
                    capture_network_request.push(capture_resource[i])
                }
            }
            return capture_network_request;
        }
        captureNetworkRequests();
      }
    },
    error: function(message) {
      messagePayload = {
        type : "error",
        message : text,
        metadata : ""
      }
      utilities.transmit(messagePayload);
      oldConsole.error(text);
    },
    track: function(fn, argsArray) {
      argsArray = argsArray ? argsArray : [];
      function wrapErrors(fn) {
        // don't wrap function more than once
        if (!fn.__wrapped__) {
          fn.__wrapped__ = function () {
            try {
              return fn.apply(this, arguments);
            } catch (e) {
              trackAndTransmit(e); // report the error
              throw e; // re-throw the error
            }
          };
        }

        return fn.__wrapped__;
      }

      var invoke = wrapErrors(function(obj, method, args) {
        return obj[method].apply(this, args);
      });

      function trackAndTransmit(ex) {
        var errorData = {
          name: ex.name,
          message: ex.line,
          url: document.location.href,
          type: "error",
          stack: ex.stack
        };
        utilities.transmit(messagePayload);
      }

      invoke(window, fn, argsArray);
    }
  }
})()

export default zapTracer;
